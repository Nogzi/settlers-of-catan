import java.util.Scanner;

public class Main {

    public static void  main(String [] args){
        int AmountOfPlayers = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println("Velkommen til Felix' Settlers Of Catan ripoff ;) \n");
        System.out.println("Hvor mange spiller skal der være (2-4)");

        while (AmountOfPlayers < 3  || AmountOfPlayers > 4) {
            AmountOfPlayers = sc.nextInt();
            switch (AmountOfPlayers) {
                case 3:
                    System.out.println("Spillet er nu sat til 3 spillere");
                    break;
                case 4:
                    System.out.println("Spillet er nu sat til 4 spillere");
                    break;
                default:
                    System.out.println("Du har valgt et ugyldigt antal spillere");
            }
        }
        for (int i = 0; i<AmountOfPlayers; i++)
            {
                Setup setup = new Setup();
                setup.SetupPlayer(i);
            }
    }
}
