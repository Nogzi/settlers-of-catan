package GameObjects;

import java.awt.*;

public class Players
{
	String name;
	int PlayerNumber;
	Color PlayerColor;

	public Players(String name, int playerNumber, Color playerColor)
	{
		this.name = name;
		PlayerNumber = playerNumber;
		PlayerColor = playerColor;
	}
}
