import GameObjects.Players;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static java.util.Map.*;

public class Setup
{
	public Players SetupPlayer(int PlayerNumber)
	{
		String PlayerName;
		Color PlayerColor = Color.white;
		Scanner sc = new Scanner(System.in);

		System.out.println("Hvad er dit navn spiller " + PlayerNumber + "?\n");
		PlayerName = sc.nextLine();

		System.out.println("Ok "+ PlayerName + " hvilken farve vil du gerne have?\n");
		System.out.println("Du kan vælge mellem GRØN, BLÅ, RØD og GUL");

		Map<String, Color> colorMap = ofEntries(
				entry("GRØN", Color.green),
				entry("BLÅ", Color.blue),
				entry("RØD", Color.red),
				entry("GUL", Color.yellow)
		);

		while (PlayerColor == Color.white)
		{
			String next = sc.next();
			Color c = colorMap.get(next);
			if (!colorMap.containsValue(c))
			{
				System.out.println("Den valgte farve er ikke en gyldig farve");
			}else{
				PlayerColor = c;
			}
		}
		Players player = new Players(PlayerName,PlayerNumber,PlayerColor);

		return player;
	}
}
